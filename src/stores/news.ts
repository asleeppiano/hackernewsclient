import { observable, decorate, action, runInAction, computed } from "mobx";
import firebase from "firebase/app";
import "firebase/database";

export enum ItemType {
  JOB = "job",
  STORY = "story",
  COMMENT = "comment",
  POLL = "poll",
  POLLOPT = "pollopt"
}

export interface Item {
  id: number;
  deleted?: boolean;
  type: ItemType;
  by: string;
  time: number;
  text: string;
  dead?: boolean;
  parent?: number;
  poll?: number;
  kids?: Array<number>;
  url: string;
  score: number;
  title: string;
  parts?: Array<number>;
  descendants?: number;
}

export interface User {
  id: string;
  delay: number;
  created: Date;
  karma: number;
  about: string;
  submitted: Array<number>;
}

export interface Story {
  id: number;
  title: string;
  score: number;
  time: number;
  by: string;
  comments: MessageTree | undefined;
}

export enum FetchState {
  PENDING,
  DONE,
  ERROR
}

export class MessageNode {
  data: any;
  parent: any;
  children: any;
  constructor(data: any, parent: any, children: any) {
    this.data = data;
    this.parent = parent;
    this.children = children;
  }
}
decorate(MessageNode, {
  data: observable,
  parent: observable,
  children: observable
});

export class MessageTree {
  _root: MessageNode;
  constructor(node: MessageNode) {
    this._root = node;
  }
}

decorate(MessageTree, {
  _root: observable
});

const POSTS_PER_PAGE = 30;

export class NewsStore {
  _fbdb: firebase.database.Reference;
  _newsList: Array<Item> = [];
  _story: Item = {} as Item;
  _newStories: Array<Item> = [];
  _comments: MessageNode = {} as MessageNode;
  _fetchState: FetchState = FetchState.PENDING;
  _fetchStoryState: FetchState = FetchState.PENDING;
  _fetchNewStoriesState = FetchState.PENDING;
  constructor() {
    try {
      firebase.initializeApp({
        databaseURL: "https://hacker-news.firebaseio.com"
      });
    } catch (err) {
      // we skip the "already exists" message which is
      // not an actual error when we're hot-reloading
      if (!/already exists/.test(err.message)) {
        console.error("Firebase initialization error", err.stack);
      }
    }
    this._fbdb = firebase.database().ref("v0");
  }

  get story(): Item {
    return this._story;
  }

  get comments(): MessageNode {
    return this._comments;
  }

  setStory(story: any): any {
    this._story = story;
  }

  addNews(news: Item) {
    this._newsList.push(news);
  }

  async fetchMessage(item: Item, tree: MessageNode, fetches: any) {
    if (item && item.kids) {
      item.kids.map(async (kid: number, index: number) => {
        try {
          const res = await this._fbdb.child(`item/${kid}`).once('value')
          const msg = res.val()
          if(msg.parent === tree.data.id) {
            runInAction(() => {
              tree.children.push(new MessageNode(msg, msg.parent, []))
            })
          }
          if(msg.kids) {
            this.fetchMessage(msg, tree.children[tree.children.length - 1], fetches)
          }
        } catch(e) {
          runInAction(() => {
            this._fetchStoryState = FetchState.ERROR
          })
        }
      });
    }
  }
  async fetchStoryComments(id: string | null) {
    console.log("id", id);
    this._fetchStoryState = FetchState.PENDING;
    try {
      const res = await this._fbdb.child(`item/${id}`).once('value')
      const json = res.val()
      let msgTree = new MessageTree(new MessageNode(json, null, []))
      let fetches: any = []
      this.fetchMessage(res.val(), msgTree._root, fetches)
      runInAction(() => {
        this._story = json
        this._comments = msgTree._root
        this._fetchStoryState = FetchState.DONE
      })
    } catch(e) {
      runInAction(() => {
        this._fetchStoryState = FetchState.ERROR
      })
    }
  }
  fetchStory(storyType: string, page: number) {
    this._fetchState = FetchState.PENDING;
    let arr: any = [];
    this._fbdb
      .child(`${storyType}`)
      .once("value")
      .then((s: any) => {
        Promise.all(
          s
            .val()
            .slice(0 + page * POSTS_PER_PAGE, (page + 1) * POSTS_PER_PAGE)
            .map((id: number) => {
              return this._fbdb.child(`item/${id}`).once("value");
            })
        )
          .then((postidList: any) => {
            postidList.map((s: firebase.database.DataSnapshot) => {
              arr.push(s.val());
            });
          })
          .then(() => {
            console.log("arr", arr);
            runInAction(() => {
              this._newsList = arr;
              this._fetchState = FetchState.DONE;
            });
          }).catch((e: any) => {
            runInAction(() => {
              this._fetchState = FetchState.ERROR;
            });
          });
      }).catch((e: any) => {
          runInAction(() => {
            this._fetchState = FetchState.ERROR;
          });
      });
  }
  get newsList() {
    return this._newsList;
  }
  setNewsList(newsList: Array<Item>) {
    console.log("setted");
    this._newsList = newsList;
  }
}
decorate(NewsStore, {
  _fetchStoryState: observable,
  _fetchState: observable,
  _story: observable,
  _comments: observable,
  _newsList: observable,
  setStory: action,
  addNews: action,
  setNewsList: action,
  fetchMessage: action,
  fetchStoryComments: action,
  fetchStory: action
});
