import { UiStore } from './ui'
import { NewsStore } from './news'

export class RootStore {
  uiStore: UiStore
  newsStore: NewsStore
  constructor() {
    this.uiStore = new UiStore()
    this.newsStore = new NewsStore()
  }
}
