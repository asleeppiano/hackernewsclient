import { decorate, computed, observable, action } from 'mobx'

export enum Pages {
  TOP,
  NEW,
  ASK,
  SHOW,
  JOB,
  COMMENTS
}

export class UiStore {
  
  private _showSideNav = observable.box(false)
  private _page = observable.box(0)
  private _currentSection = observable.box(Pages.TOP)

  set currentSection(currentSection: Pages) {
    this._currentSection.set(currentSection)
  }

  get currentSection() {
    return this._currentSection.get()
  }

  setShowSideNav() {
    const currShowSideNav = this._showSideNav.get()
    this._showSideNav.set(!currShowSideNav)
  }

  get showSideNav() {
    return this._showSideNav.get()
  }

  showNext() {
    const currPage = this._page.get()
    this._page.set(currPage + 1)
  }
  showPrev() {
    const currPage = this._page.get()
    if(currPage > 0)
      this._page.set(currPage - 1)
  }
  set page(num: number) {
    this._page.set(num)
  }
  get getPage() {
    return this._page.get()
  }
}
decorate(UiStore, {
  showNext: action,
  showPrev: action,
  setShowSideNav: action
})
