import React, { useState, useEffect, ReactNode } from "react";
import "./comments-list.scss";
import { MessageTree, MessageNode } from '../../stores/news'
import { observer } from 'mobx-react'
import Comment from "../comment/comment";

type Props = {
  treeNode: MessageNode | undefined
  commentsCount: any
};

const CommentsList: React.FC<Props> = observer(({treeNode, commentsCount}) => {
  useEffect(() => {
    commentsCount++
  })
  return (
    <ul>
      {treeNode && treeNode.children && treeNode.children.map((node: MessageNode, index: number) => {
      console.log('data', node.data.time)
        return (
          <>
            <Comment key={node.data.id} time={node.data.time} text={node.data.text} author={node.data.by} />
              {node.children && node.children.length > 0 ? <CommentsList commentsCount={commentsCount} key={node.data} treeNode={node}/> : ''}
          </>
        )
      })}
    </ul>
  );
});
export default CommentsList;

