import React, { ReactNode } from "react";
import "./layout.scss";
import Header from "../header/header";
import BottomNav from '../bottom-nav/bottom-nav'
import SideNav from '../sidenav/sidenav'
import { observer } from 'mobx-react'
import { useStore } from '../../store'

type Props = {
  children: ReactNode;
};

const Layout: React.FC<Props> = observer(({ children }) => {
  const store = useStore()
  return (
    <div className="layout">
      <Header grid="grid-header" />
      <main className="grid-main">{children}</main>
      <SideNav open={store.uiStore.showSideNav} />
      <BottomNav grid="grid-footer" />
    </div>
  );
});
export default Layout;
