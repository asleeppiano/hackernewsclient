import React, { useEffect, useState, useRef } from "react";
import "./posts-list.scss";
import Post from "../post/post";
import Loading from "../loading/loading";
import { observer } from "mobx-react";
import { useStore } from "../../store";
import { FetchState } from "../../stores/news";
import { Pages } from "../../stores/ui";

type Props = {
  listType: string;
};
const PostsList: React.FC<Props> = observer(({ listType }) => {
  const store = useStore();
  const postListRef = useRef<HTMLDivElement>(null);
  useEffect(() => {
    switch (listType) {
      case "top":
        store.newsStore.fetchStory("topstories", store.uiStore.getPage);
        store.uiStore.currentSection = Pages.TOP;
        break;
      case "new":
        store.newsStore.fetchStory("newstories", store.uiStore.getPage);
        store.uiStore.currentSection = Pages.NEW;
        break;
      case "ask":
        store.newsStore.fetchStory("askstories", store.uiStore.getPage);
        store.uiStore.currentSection = Pages.ASK;
        break;
      case "show":
        store.newsStore.fetchStory("showstories", store.uiStore.getPage);
        store.uiStore.currentSection = Pages.SHOW;
        break;
      case "job":
        store.newsStore.fetchStory("jobstories", store.uiStore.getPage);
        store.uiStore.currentSection = Pages.JOB;
        break;
      default:
        console.log("err");
    }
    console.log('here')
  }, [listType, store.uiStore.getPage]);

  useEffect(() => {
    console.log("store newstories = ", store.newsStore.newsList);
    console.log(
      "postlistref",
      postListRef.current ? postListRef.current.getBoundingClientRect() : ""
    );
  });

  function showNext() {
    console.log("show next");
    store.uiStore.showNext();
  }
  function showPrev() {
    store.uiStore.showPrev();
  }
  function postListScroll(e: any) {
    console.log(e.target.scrollTop);
  }
  return (
    <div
      id="postList"
      ref={postListRef}
      onScroll={postListScroll}
      className="posts-list"
    >
      {store.newsStore._fetchState === FetchState.DONE &&
        store.newsStore.newsList.map((news, index) => {
          if (news) {
            return (
              <Post
                itemid={news.id}
                num={30 * store.uiStore.getPage + index + 1}
                title={news.title}
                time={news.time}
                by={news.by}
                url={news.url}
                score={news.score}
                key={news.id}
              />
            );
          }
          return "";
        })}
      {store.newsStore._fetchState === FetchState.PENDING && <Loading />}
      {store.newsStore._fetchState === FetchState.ERROR && "ERROR..."}
      <div className="posts-list__controls">
        <button onClick={showPrev} className="posts-list__button">
          prev{" "}
        </button>
        <button onClick={showNext} className="posts-list__button">
          next{" "}
        </button>
      </div>
    </div>
  );
});

export default PostsList;
