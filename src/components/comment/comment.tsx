import React, { useState, useEffect } from "react";
import "./comment.scss";

type Props = {
  author: string;
  time: number;
  text: string;
};

const Comment: React.FC<Props> = ({ author, time, text }: Props) => {
  const date = new Date(parseInt(time + '000'));
  return (
    <li className={`comment`}>
      <div className="comment-top">
        <div className="comment__meta">
          <h4 className="comment__author">{author}</h4>
          <time className="comment__time">
            {date.getHours() +
              ":" +
              date.getMinutes() +
              "  " +
              date.getDate() +
              ":" +
              (date.getMonth() + 1) +
              ":" +
              date.getFullYear()}
          </time>
        </div>
      </div>
      <div
        dangerouslySetInnerHTML={{ __html: text }}
        className="comment__text"
      ></div>
    </li>
  );
};
export default Comment;
