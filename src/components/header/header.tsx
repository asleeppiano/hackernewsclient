import React from 'react'
import './header.scss'
import { observer } from "mobx-react"
import { useStore } from '../../store'
import { Pages } from '../../stores/ui'
import { BrowserRouter as Router, Link } from 'react-router-dom'

type Props = {
 grid: string; 
}

const Header: React.FC<Props> = observer(({grid}) => {
  const store = useStore()
  function openSidebar() {
    store.uiStore.setShowSideNav()
  }
  function setCurrentSection(e: any) {
    store.uiStore.page = 0
    store.uiStore.currentSection = e.target.dataset.value
  }
  return (
    <div className={`header ${grid}`}>
      <Link data-value={Pages.TOP} onClick={setCurrentSection} to="/"><h1 className="header__app-title">Hacker Client</h1></Link>
      <ul className="nav-list">
        <li className="nav-list__link"><Link data-value={Pages.NEW} onClick={setCurrentSection} to="/new">new</Link></li>
        <li className="nav-list__link nav-list__link--tbd"><div>past</div></li>
        <li className="nav-list__link nav-list__link--tbd"><div>comments</div></li>
          <li className="nav-list__link"><Link data-value={Pages.ASK} onClick={setCurrentSection} to="/ask">ask</Link></li>
            <li className="nav-list__link" ><Link data-value={Pages.SHOW} onClick={setCurrentSection} to="/show">show</Link></li>
            <li className="nav-list__link"><Link data-value={Pages.JOB} onClick={setCurrentSection} to="/job">jobs</Link></li>
        <li className="nav-list__link nav-list__link--tbd"><div>lists</div></li>
      </ul>
      <div onClick={openSidebar} className="header__menu">
        <span></span>
        <span></span>
        <span></span>
      </div>
    </div>
  )
})
export default Header
