import React, { useState } from "react";
import "./loading.scss";

type Props = {};

const Loading: React.FC<Props> = () => {
  return (
    <div className="loading">
      <div className="lds-ellipsis">
        <div></div>
        <div></div>
        <div></div>
        <div></div>
      </div>
    </div>
  );
};
export default Loading;
