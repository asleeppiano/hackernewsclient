import React, { useEffect, useState } from "react";
import "./post-page.scss";
import Layout from "../layout/layout";
import CommentsList from "../comments-list/comments-list";
import Loading from "../loading/loading"
import { observer } from "mobx-react";
import { useStore } from "../../store";
import { API_ADDR } from "../../constants";
import { useParams, useLocation } from "react-router-dom";
import { FetchState, MessageNode } from "../../stores/news";

type Props = {
};

function useQuery() {
  return new URLSearchParams(useLocation().search);
}

const PostPage: React.FC<Props> = observer(() => {
  console.log("post page");
  const query = useQuery();
  const store = useStore();
  let commentsCount = 0

  useEffect(() => {
    store.newsStore.fetchStoryComments(query.get("id"));
  }, []);
  useEffect(() => {
    console.log('story:', store.newsStore.story)
  })
  const date = new Date(new Date().getTime() - store.newsStore.story.time)
  return (
    <Layout>
      {store.newsStore._fetchStoryState === FetchState.DONE ? (
        <div className={`post-page`}>
          <div className="post-page-top">
            <h2 className="post-page__title">{store.newsStore.story.title}</h2>
            <div className="post-page__meta">
              <div className="post-page__upvotes">
                {store.newsStore.story.score}
              </div>
              <div className="post-page__time">
                {date.getFullYear() + "/" + (date.getMonth() ? date.getMonth() : '12') + "/" + date.getDate() + " " + date.getHours() + ":" + date.getMinutes()}
              </div>
              <div className="post-page__author">
                {store.newsStore.story.by}
              </div>
            </div>
          </div>
          <div id="commentsList" className="post-page__comments-container">
            <CommentsList commentsCount={commentsCount} treeNode={store.newsStore.comments} />
          </div>
        </div>
      ) : (
        ""
      )}
      {store.newsStore._fetchStoryState === FetchState.PENDING ? (
          <Loading />
      ) : (
        ""
      )}
      {store.newsStore._fetchStoryState === FetchState.ERROR ? (
        <div> ERROR... </div>
      ) : (
        ""
      )}
    </Layout>
  );
});
export default PostPage;
