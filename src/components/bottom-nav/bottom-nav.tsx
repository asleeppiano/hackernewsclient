import React, { ReactNode } from "react";
import "./bottom-nav.scss";
import { observer } from 'mobx-react'
import { useStore } from "../../store";
import { Pages } from '../../stores/ui'
import {useHistory} from 'react-router-dom'

type Props = {
  grid: string;
};

const BottomNav: React.FC<Props> = observer(({ grid }) => {
  const store = useStore();
  const history = useHistory()
  function showNext() {
    store.uiStore.showNext();
  }
  function showPrev() {
    if(store.uiStore.currentSection !== Pages.COMMENTS) {
      store.uiStore.showPrev();
    } else {
      history.goBack()
    }
  }
  function scrollTop() {
    if(store.uiStore.currentSection !== Pages.COMMENTS) {
      const postList = document.getElementById('postList')
      if(postList)
        postList.scrollTop = 0
    } else {
      const commentsList = document.getElementById('commentsList')
      if(commentsList)
        commentsList.scrollTop = 0
    }
  }
  return (
    <div className={`bottom-nav ${grid}`}>
      <button onClick={showPrev} className="bottom-nav__prev">
        <svg
          xmlns="http://www.w3.org/2000/svg"
          width="24"
          height="24"
          viewBox="0 0 24 24"
        >
          <path d="M15.41 7.41L14 6l-6 6 6 6 1.41-1.41L10.83 12z" />
          <path d="M0 0h24v24H0z" fill="none" />
        </svg>
      </button>
      <button onClick={scrollTop} className="bottom-nav__top">
        <svg
          xmlns="http://www.w3.org/2000/svg"
          width="24"
          height="24"
          viewBox="0 0 24 24"
        >
          <path fill="none" d="M0 0h24v24H0V0z" />
          <path d="M4 12l1.41 1.41L11 7.83V20h2V7.83l5.58 5.59L20 12l-8-8-8 8z" />
        </svg>
      </button>
        {store.uiStore.currentSection !== Pages.COMMENTS ? 
      <button onClick={showNext} className="bottom-nav__next">
        <svg
          xmlns="http://www.w3.org/2000/svg"
          width="24"
          height="24"
          viewBox="0 0 24 24"
        >
          <path d="M10 6L8.59 7.41 13.17 12l-4.58 4.59L10 18l6-6z" />
          <path d="M0 0h24v24H0z" fill="none" />
        </svg>
      </button> : ''}
    </div>
  );
});
export default BottomNav;
