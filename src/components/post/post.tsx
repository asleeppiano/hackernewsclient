import React, { useState } from "react";
import { Item } from "../../stores/news";
import { Link, Redirect, Router } from 'react-router-dom'
import { observer } from 'mobx-react'
import { useStore } from '../../store'
import "./post.scss";
import { MessageTree, MessageNode, Story } from '../../stores/news'
import { Pages } from '../../stores/ui'

type Props = {
  itemid: number;
  title: string; time: number;
  by: string;
  url: string;
  score: number;
  num: number;
};

const Post: React.FC<Props> = observer(({ itemid, num, title, time, by, url, score }) => {
  const date = new Date(parseInt(time + '000'));
  const store = useStore()
  const [redirect, setRedirect] = useState(false)

  function showComments(){
    console.log('showComments')
    store.uiStore.currentSection = Pages.COMMENTS
    store.newsStore.setStory({ itemid , title, score, time, by, comments: undefined })
    setRedirect(true)
  }
  function renderRedirect() {
    if(redirect) {
      console.log('renderRedirect')
      return <Redirect push to={`/posts?id=${itemid}`}/>
    }
  }
  return (
    <>
    {renderRedirect()}
    <div className="post">
      <a href={url} className="post__name post--name">
        {num}. {title}
      </a>
      <div className="post__meta post--meta">
        <div className="post__likes post__meta--item">{score}</div>
        <div className="post__time post__meta--item">
        {date.getHours() + ":" + date.getMinutes() + "  " + date.getDate()  + "/" + (date.getMonth() + 1) + "/" + date.getFullYear()}
        </div>
        <div className="post__author post__meta--item">{by}</div>
      </div>
      <button onClick={showComments} className="post__comments post--comments">comments</button>
    </div>
    </>
  );
});
export default Post;
