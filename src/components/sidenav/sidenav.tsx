import React, { ReactNode, useEffect, useRef } from "react";
import "./sidenav.scss";
import { tween, styler, stagger } from 'popmotion'
import { useStore } from '../../store'
import { observer } from 'mobx-react'
import {BrowserRouter as Router, Link } from 'react-router-dom'

type Props = {
  open: boolean;
};

const SideNav: React.FC<Props> = observer(({open}) => {
  const sidenavRef = useRef<HTMLDivElement>(null)
  const store = useStore()
  useEffect(() => {
    const sidenavStyler = styler(sidenavRef.current as Element);
    if(open) {
      tween({
        to: '100%',
        duration: 600
      }).start((v: any) => sidenavStyler.set('x', v))
    } else if(sidenavStyler.get('x') === 0){
      console.log('equals 0')
    } else {
      tween({
        from: '100%',
        to: '0%',
        duration: 600
      }).start((v: any) => sidenavStyler.set('x', v))
    }
  }) 
  function closeSideNav() {
    console.log('here')
    store.uiStore.setShowSideNav()
    store.uiStore.page = 0
  }
  return (
    <nav ref={sidenavRef} className="sidenav">
      <div onClick={closeSideNav} className="sidenav__close">
        <span></span>
        <span></span>
      </div>
      <ul className="sidenav-list">
        <li><Link className="sidenav__link" onClick={closeSideNav} to="/new">new</Link></li>
        <li><div className="sidenav__link sidenav__link--tbd">past</div></li>
        <li><div className="sidenav__link sidenav__link--tbd">comments</div></li>
          <li><Link className="sidenav__link" onClick={closeSideNav} to="/ask">ask</Link></li>
            <li><Link className="sidenav__link" onClick={closeSideNav} to="/show">show</Link></li>
            <li><Link className="sidenav__link" onClick={closeSideNav} to="/job">jobs</Link></li>
        <li><div className="sidenav__link sidenav__link--tbd">lists</div></li>
      </ul>
      <div className="sidenav-footer">
        <span className="sidenav-footer__item sidenav-footer__item--tbd">Guidlines</span>
        <span className="sidenav-footer__item sidenav-footer__item--tbd">API</span>
        <span className="sidenav-footer__item sidenav-footer__item--tbd">FAQ</span>
        <span className="sidenav-footer__item sidenav-footer__item--tbd">Support</span>
        <span className="sidenav-footer__item sidenav-footer__item--tbd">Security</span>
        <span className="sidenav-footer__item sidenav-footer__item--tbd">Bookmarket</span>
        <span className="sidenav-footer__item sidenav-footer__item--tbd">Legal</span>
        <span className="sidenav-footer__item sidenav-footer__item--tbd">Contact</span>
        <span className="sidenav-footer__item sidenav-footer__item--tbd">Apply to YC</span>
      </div>
    </nav>
  );
});
export default SideNav;
