import React from 'react'
import './home.scss'
import Layout from "../layout/layout";
import PostsList from "../posts-list/posts-list";
import { observer } from "mobx-react"

type Props = {
  listType: string
}

const Home: React.FC<Props> = observer(({listType}) => {
  return (
    <div className={`home`}>
      <Layout>
        <PostsList listType={listType} />
      </Layout>
    </div>
  )
})
export default Home

