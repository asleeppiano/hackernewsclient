import React, { useState, ReactNode } from "react";
import "./App.scss";
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";
import Home from "./components/home/home";
import PostPage from "./components/post-page/post-page";
import { observer } from "mobx-react";
import { StoresContext, useStore } from "./store";

const App: React.FC = observer(() => {
  const store: any = useStore();

  return (
    <Router>
      <StoresContext.Provider value={store}>
        <Switch>
          <Route exact path="/">
            <Home listType="top" />
          </Route>
          <Route path="/posts">
            <PostPage />
          </Route>
          <Route path="/new">
            <Home listType="new" />
          </Route>
          <Route path="/ask">
            <Home listType="ask" />
          </Route>
          <Route path="/show">
            <Home listType="show" />
          </Route>
          <Route path="/job">
            <Home listType="job" />
          </Route>
        </Switch>
      </StoresContext.Provider>
    </Router>
  );
});

export default App;
